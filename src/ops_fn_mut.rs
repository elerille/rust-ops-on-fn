use crate::not;

pub trait OpsFnMut<Args> {
    fn not_mut(&mut self) -> not::NotMut<&mut Self> {
        not::NotMut::new(self)
    }
}
impl<T, Args> OpsFnMut<Args> for T where T: FnMut<Args> {}

#[cfg(test)]
mod tests {

    #[test]
    fn not_mut() {
        use super::*;
        let mut ctr = 0;
        let mut f1 = |a: bool| {
            ctr += 1;
            a
        };
        assert_eq!(f1(true), true);
        assert_eq!(f1(false), false);
        assert_eq!(&ctr, &2);

        let mut ctr = 0;
        let mut f1 = |a: bool| {
            ctr += 1;
            a
        };
        let mut not_f1 = f1.not_mut();
        assert_eq!(not_f1(true), false);
        assert_eq!(not_f1(false), true);
        assert_eq!(&ctr, &2);
    }
}
