use crate::not;

pub trait OpsFnOnce<Args>
where
    Self: Sized,
{
    fn not_once(self) -> not::NotOnce<Self> {
        not::NotOnce::new(self)
    }
}
impl<T, Args> OpsFnOnce<Args> for T where T: FnOnce<Args> {}

#[cfg(test)]
mod tests {
    use std::ops;

    #[derive(Debug, PartialEq)]
    struct A(i32);
    impl ops::Not for A {
        type Output = Self;
        fn not(self) -> Self::Output {
            if self.0 == 0 {
                Self(1)
            } else {
                Self(0)
            }
        }
    }

    #[test]
    fn not_once() {
        use super::*;
        let r = A(2);
        let f1 = move || r;
        assert_eq!(f1(), A(2));
        let r = A(2);
        let f1 = move || r;
        let not_f1 = f1.not_once();
        assert_eq!(not_f1(), A(0));
    }
}
