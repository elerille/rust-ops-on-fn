use std::ops;

pub struct Not<T>(T)
where
    T: Sized;

impl<T> Not<T> {
    pub fn new(a: T) -> Self {
        Self(a)
    }
}

impl<T, Args> FnOnce<Args> for Not<T>
where
    T: FnOnce<Args> + Sized,
    T::Output: ops::Not,
{
    type Output = <T::Output as ops::Not>::Output;
    extern "rust-call" fn call_once(self, args: Args) -> Self::Output {
        !self.0.call_once(args)
    }
}

impl<T, Args> FnMut<Args> for Not<T>
where
    T: FnMut<Args> + Sized,
    T::Output: ops::Not,
{
    extern "rust-call" fn call_mut(&mut self, args: Args) -> Self::Output {
        !self.0.call_mut(args)
    }
}

impl<T, Args> Fn<Args> for Not<T>
where
    T: Fn<Args> + Sized,
    T::Output: ops::Not,
{
    extern "rust-call" fn call(&self, args: Args) -> Self::Output {
        !self.0.call(args)
    }
}

pub struct NotMut<T>(T);
impl<T> NotMut<T> {
    pub fn new(a: T) -> Self {
        Self(a)
    }
}

impl<T, Args, O> FnOnce<Args> for NotMut<T>
where
    O: ops::Not,
    T: FnOnce<Args, Output = O>,
{
    type Output = <O as ops::Not>::Output;
    extern "rust-call" fn call_once(self, args: Args) -> Self::Output {
        !self.0.call_once(args)
    }
}

impl<T, Args, O> FnMut<Args> for NotMut<T>
where
    O: ops::Not,
    T: FnMut<Args, Output = O>,
{
    extern "rust-call" fn call_mut(&mut self, args: Args) -> Self::Output {
        !self.0.call_mut(args)
    }
}

pub struct NotOnce<T>(T);
impl<T> NotOnce<T> {
    pub fn new(a: T) -> Self {
        Self(a)
    }
}

impl<T, Args, O> FnOnce<Args> for NotOnce<T>
where
    O: ops::Not,
    T: FnOnce<Args, Output = O>,
{
    type Output = <O as ops::Not>::Output;
    extern "rust-call" fn call_once(self, args: Args) -> Self::Output {
        !self.0.call_once(args)
    }
}

#[cfg(test)]
mod internals_tests {
    use std::ops;

    #[derive(Debug, PartialEq)]
    struct A(i32);
    impl ops::Not for A {
        type Output = Self;
        fn not(self) -> Self::Output {
            if self.0 == 0 {
                Self(1)
            } else {
                Self(0)
            }
        }
    }

    #[test]
    fn fn_once() {
        use super::*;
        let r = A(2);
        let f1 = move || r;
        assert_eq!(f1(), A(2));
        let r = A(2);
        let f1 = move || r;
        let not_f1 = Not(f1);
        assert_eq!(not_f1.call_once(()), A(0));
    }

    #[test]
    fn fn_mut() {
        use super::*;
        let mut ctr = 0;
        let mut f1 = |a: bool| {
            ctr += 1;
            a
        };
        assert_eq!(f1(true), true);
        assert_eq!(f1(false), false);
        assert_eq!(&ctr, &2);

        let mut ctr = 0;
        let f1 = |a: bool| {
            ctr += 1;
            a
        };
        let mut not_f1 = Not(f1);
        assert_eq!(not_f1.call_mut((true,)), false);
        assert_eq!(not_f1.call_mut((false,)), true);
        assert_eq!(&ctr, &2);
    }

    #[test]
    fn function() {
        use super::*;
        let f1 = |a| a;
        assert_eq!(f1(true), true);
        assert_eq!(f1(false), false);
        let not_f1 = Not(&f1);
        assert_eq!(not_f1(true), false);
        assert_eq!(not_f1(false), true);
    }
}
