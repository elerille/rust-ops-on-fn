use crate::not;

pub trait OpsFn<Args> {
    fn not(&self) -> not::Not<&Self>
    where
        Self: Fn<Args>,
    {
        not::Not::new(self)
    }
}
impl<T, Args> OpsFn<Args> for T where T: Fn<Args> {}

#[cfg(test)]
mod tests {
    #[test]
    fn not() {
        use super::*;
        let f1 = &|a| a;
        assert_eq!(f1(true), true);
        assert_eq!(f1(false), false);
        let not_f1 = f1.not();
        assert_eq!(not_f1(true), false);
        assert_eq!(not_f1(false), true);
    }

    #[test]
    fn not_2() {
        use super::*;
        let f1 = &|a| a;
        assert_eq!(f1(""), "");
        assert_eq!(f1(""), "");
        let _ = f1.not();
    }

    fn clone_bool(i: bool) -> bool {
        i
    }

    #[test]
    fn not_3() {
        use super::*;
        assert_eq!(clone_bool(true), true);
        assert_eq!(clone_bool(false), false);
        let not_f1 = clone_bool.not();
        assert_eq!(not_f1(true), false);
        assert_eq!(not_f1(false), true);
    }
}
