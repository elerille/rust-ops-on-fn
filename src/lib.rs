#![feature(unboxed_closures)]
#![feature(fn_traits)]

pub mod not;

mod ops_fn;
mod ops_fn_mut;
mod ops_fn_once;

pub use ops_fn::*;
pub use ops_fn_mut::*;
pub use ops_fn_once::*;

pub mod prelude {
    pub use crate::ops_fn::OpsFn;
    pub use crate::ops_fn_mut::OpsFnMut;
    pub use crate::ops_fn_once::OpsFnOnce;
}
